﻿using System;
using System.Windows.Forms;

namespace DesktopApp
{
    public partial class PalindromeFrm : Form
    {        
        static string DirectoryPath = Environment.CurrentDirectory + "\\Palindromes";
        public PalindromeChecker Checker = new PalindromeChecker(DirectoryPath);
        public string outLog = String.Empty;
        public PalindromeFrm()
        {
            InitializeComponent();
            Checker.GetWordsList(out outLog);
            txbLog.AppendText(outLog + Environment.NewLine);
        }

        private void btnStartCheck_Click(object sender, EventArgs e)
        {
            Checker.CheckWordsArr(out outLog);
            txbLog.AppendText(outLog);
        } 

        private void btnReloadFiles_Click(object sender, EventArgs e)
        {            
            outLog = String.Empty;
            Checker.GetWordsList(out outLog);
            txbLog.AppendText(outLog + Environment.NewLine);
        }
    }
}
