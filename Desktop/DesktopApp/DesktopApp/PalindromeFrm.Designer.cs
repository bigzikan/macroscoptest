﻿namespace DesktopApp
{
    partial class PalindromeFrm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txbLog = new System.Windows.Forms.TextBox();
            this.btnStartCheck = new System.Windows.Forms.Button();
            this.btnReloadFiles = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txbLog
            // 
            this.txbLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txbLog.Location = new System.Drawing.Point(21, 55);
            this.txbLog.Multiline = true;
            this.txbLog.Name = "txbLog";
            this.txbLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txbLog.Size = new System.Drawing.Size(633, 270);
            this.txbLog.TabIndex = 1;
            // 
            // btnStartCheck
            // 
            this.btnStartCheck.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStartCheck.Location = new System.Drawing.Point(557, 12);
            this.btnStartCheck.Name = "btnStartCheck";
            this.btnStartCheck.Size = new System.Drawing.Size(97, 29);
            this.btnStartCheck.TabIndex = 3;
            this.btnStartCheck.Text = "Проверить";
            this.btnStartCheck.UseVisualStyleBackColor = true;
            this.btnStartCheck.Click += new System.EventHandler(this.btnStartCheck_Click);
            // 
            // btnReloadFiles
            // 
            this.btnReloadFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReloadFiles.Location = new System.Drawing.Point(414, 12);
            this.btnReloadFiles.Name = "btnReloadFiles";
            this.btnReloadFiles.Size = new System.Drawing.Size(137, 29);
            this.btnReloadFiles.TabIndex = 4;
            this.btnReloadFiles.Text = "Перезагрузить файлы";
            this.btnReloadFiles.UseVisualStyleBackColor = true;
            this.btnReloadFiles.Click += new System.EventHandler(this.btnReloadFiles_Click);
            // 
            // PalindromeFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(666, 337);
            this.Controls.Add(this.btnReloadFiles);
            this.Controls.Add(this.btnStartCheck);
            this.Controls.Add(this.txbLog);
            this.Name = "PalindromeFrm";
            this.Text = "Palindrome Desktop";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txbLog;
        private System.Windows.Forms.Button btnStartCheck;
        private System.Windows.Forms.Button btnReloadFiles;
    }
}

