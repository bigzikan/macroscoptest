﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DesktopApp
{
    public class PalindromeChecker
    {
        private string DirectoryPath;
        private List<List<string>> WordsArr = new List<List<string>>();
        public PalindromeChecker(string Directory)
        {
            DirectoryPath = Directory;
        }

        public void GetWordsList(out string outLog)
        {            
            WordsArr.Clear();
            outLog = string.Empty;

            try
            {
                if (Directory.Exists(DirectoryPath))
                {
                    foreach (var file in Directory.GetFiles(DirectoryPath, "*.txt"))
                    {
                        foreach (var line in File.ReadLines(file))
                        {
                            WordsArr.Add(new List<string> { Path.GetFileName(file), line });
                        }
                    }
                    outLog += Environment.NewLine + "Чтение каталога успешно завершено";
                }
                else
                {
                    outLog += Environment.NewLine + "Отсутствует директория " + DirectoryPath + "! ";
                }
            }
            catch (Exception e)
            {
                outLog += Environment.NewLine + "Ошибка при чтении файлов: " + e.Message;
            };
        }

        private static string CheckValueSync(string Input)
        {
            string url = "https://localhost:44313/api/Palindrome/IsPalindrome";
            var response = String.Empty;
            using (var webClient = new WebClient())
            {
                // Создаём коллекцию параметров
                var pars = new NameValueCollection();

                // Добавляем необходимые параметры в виде пар ключ, значение
                pars.Add("PalindromeString", Input);
                string parsStr = "{ \"PalindromeString\": " + "\"" + Input + "\"}";
                // Посылаем параметры на сервер
                // Может быть ответ в виде массива байт
                webClient.Encoding = System.Text.Encoding.UTF8;
                webClient.Headers.Add("content-type", "application/json");

                response = webClient.UploadString(url, "POST", parsStr);
            }
            return response;
        }

        class PalindromeJson
        {
            public string PalindromeString { get; set; }
        }

        private static async Task<string> CheckValue(string Input)
        {
            string result = String.Empty;
            try
            {                
                // Создаём объект WebClient
                using (var webClient = new HttpClient())
                {
                    var palindrome = new PalindromeJson();
                    palindrome.PalindromeString = Input;

                    var json = JsonConvert.SerializeObject(palindrome);
                    var requestBody = new StringContent(json, Encoding.UTF8, "application/json");

                    // Выполняем запрос по адресу и получаем ответ в виде строки
                    var response = webClient.PostAsync("https://localhost:44313/api/Palindrome/IsPalindrome", requestBody).Result;

                    response.EnsureSuccessStatusCode();

                    result = await response.Content.ReadAsStringAsync();
                }
                return result;
            }
            catch (Exception e)
            {
                if (e.Message.Contains("429"))
                {
                    result += Input + " - " + e.Message + Environment.NewLine;
                    System.Threading.Thread.Sleep(1000);
                    Task<string> task = CheckValue(Input);
                    return result += task.Result;
                }
                else
                    return e.Message;
            }
        }

        public void CheckWordsArr(out string outLog)
        {
            outLog = String.Empty;
            foreach (List<string> word in WordsArr)
            {
                Task<string> task = CheckValue(word[1]);
                outLog += Environment.NewLine + word[0] + " - " + task.Result;
                //string response = CheckValueSync(word[1]);
                //txbLog.AppendText(word[0] + " - " + response + Environment.NewLine);
            }
        }
    }
}
