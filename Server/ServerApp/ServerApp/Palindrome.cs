﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServerApp
{
    public class Palindrome
    {
        public class PalindromeValue
        {
            public string PalindromeString { get; set; }
        }

        public bool IsPalindrome(PalindromeValue InputValue, out string ResponseMessage)
        {
            bool Result =  true;
            ResponseMessage = null;
            try
            {
                string PalindromeString = InputValue.PalindromeString.ToUpper();
                PalindromeString = PalindromeString.Replace(" ", String.Empty);

                if (String.IsNullOrEmpty(PalindromeString))
                    throw new Exception("Значение не может быть пустым или содержать только пробелы!");

                string ControlString = PalindromeString;
                PalindromeString = new string(PalindromeString.ToCharArray().Reverse().ToArray());
                if (String.Compare(ControlString, PalindromeString) != 0)
                    Result = false;

                //System.Threading.Thread.Sleep(1000);
            }
            catch(Exception e)
            {
                Result = false;
                ResponseMessage = e.Message;
            };
            return Result;
        }
    }
}
