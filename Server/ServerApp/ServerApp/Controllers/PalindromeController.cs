﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ServerApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PalindromeController : ControllerBase
    {
        private readonly ILogger<PalindromeController> _logger;

        public PalindromeController(ILogger<PalindromeController> logger)
        {
            _logger = logger;
        }

        [Serializable]
        public class PalindromeAnswer
        {
            public string IsPalindrome { get; set; }
        }

        [HttpPost("IsPalindrome")]
        public string IsPalindrome([FromBody] Palindrome.PalindromeValue PalindromeValue)
        {
            Palindrome PalindromeEx = new Palindrome();
            string Result = "Yes";
            string ResponseMessage = null;
            try
            {
                if (!PalindromeEx.IsPalindrome(PalindromeValue, out ResponseMessage))
                {
                    if (ResponseMessage != null)
                        Result = ResponseMessage;
                    else
                        Result = "No";
                }
            }
            catch
            {
                Result = "No";
            }

            PalindromeAnswer ResultJSON = new PalindromeAnswer { IsPalindrome = Result };
            string json = JsonSerializer.Serialize<PalindromeAnswer>(ResultJSON);
            return json;
        }        
    }
}
